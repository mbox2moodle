From isabelle.hurbain@free.fr  Thu Jul 18 07:44:32 2002
Return-Path: <isabelle.hurbain@free.fr>
Delivered-To: courses@linuxchix.org
Received: from mout0.freenet.de (mout0.freenet.de [194.97.50.131])
	by www.linuxchix.org (Postfix) with ESMTP id 3632712585C
	for <courses@linuxchix.org>; Thu, 18 Jul 2002 07:44:31 +1000 (EST)
Received: from [194.97.50.136] (helo=mx3.freenet.de)
	by mout0.freenet.de with esmtp (Exim 4.05)
	id 17Uwav-0007qi-00
	for courses@linuxchix.org; Wed, 17 Jul 2002 23:44:25 +0200
Received: from b120d.pppool.de ([213.7.18.13] helo=isa.free.fr)
	by mx3.freenet.de with esmtp (Exim 4.05 #1)
	id 17Uwat-0004yP-00
	for courses@linuxchix.org; Wed, 17 Jul 2002 23:44:24 +0200
Message-Id: <5.1.0.14.0.20020717233539.024673e8@pop.free.fr>
X-Sender: isabelle.hurbain@pop.free.fr
X-Mailer: QUALCOMM Windows Eudora Version 5.1
Date: Wed, 17 Jul 2002 23:44:52 +0200
To: courses@linuxchix.org
From: Isabelle Hurbain <isabelle.hurbain@free.fr>
Mime-Version: 1.0
Content-Type: text/plain; charset="us-ascii"; format=flowed
Subject: [Courses] [LaTeX] Course 1 - Introduction
Sender: courses-admin@linuxchix.org
Errors-To: courses-admin@linuxchix.org
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.0.1
Precedence: bulk
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
List-Archive: <http://mailman.linuxchix.org/pipermail/courses/>
Content-Length: 6497
Lines: 182

Hello everyone,

I suggested three days ago on the IRC channel to run LaTeX courses, and as 
I had positive feedback about it, here is the first course ;)
This is text only ; you can have the PDF/TeX/DVI version on my website : 
<http://www.isabelle-hurbain.com/repository/latex/courses/>.
I hope you'll enjoy it ! Please do not hesitate to contact me for any 
question - or mistake (I've tried to make this as error proof as I could, 
but English is not my mother tongue...)

Isa - Balise



LaTeX course 1 : Introduction

Contents

1. But what is LaTeX anyway ?
2. What do I need to make LaTeX ?
3. LaTeX Hello World



1. But what is LaTeX anyway ?

LaTeX is a document preparation system. It allows you to create 
professional-looking documents without pain. The main idea of LaTeX is that 
the author of the document concentrates on the content and not the form of 
the document. To achieve this, LaTeX provides a set of macros and 
predefined styles.

Let's take a basic example. On a "standard" word-processing tool, to make a 
section title, most people use form-modifying commands. For example, they 
put it in Bold, Underlined, Size 16. With LaTeX, the form of your document 
is independant of its content : your section title will be in a \section 
command and you let LaTeX manage how it will be printed and diplayed.

You may ask : "What's the big point ?" Well, there are indeed at least 
three big points. The first is obviously that you don't have to remember 
what "style" you used for your previous sections, subsections, chapters and 
so on. The second is that when you decide that your section title font is 
too big, you don't have to change it in the whole document, you can do it 
with a single modification. The third is that, as the document is marked 
with sections, subsections and so on, it is much easier to do tables of 
contents - in fact, it can be done automagically.

The last point I'd like to underline is that LaTeX is really fantastic to 
typeset mathematical formulaes. You will soon see that once used, you 
cannot do without LaTeX for scientific publishing ;-)

I hope that this very short introduction has given you the envy to 
continue, so let's go !

2. What do I need to make LaTeX ?

LaTeX is not, like most word-processors, WYSIWYG (What You See Is What You 
Get) (or, more frequently, WYSIMOLWYG, What You See Is More Or Less What 
You Get). You first write your LaTeX source code, then you compile it into 
a .dvi file, which you can transform in .ps or .pdf. There are also means 
to convert into HTML for example.

So what you need are basically :
	- a text editor - Emacs, Vi, TextPad, UltraEdit or whatever you like
	- a LaTeX distribution. You can find teTeX under Linux Debian, and there 
is a TeX/LaTeX distribution bundled with almost all Linux distribution. 
Under Windows, if you use Cygwin there is also a teTeX ; you can also find 
MiKTeX and TeX Live (I'm sorry for Mac users, I don't use Mac so you'll 
have to find a Mac distro yourself :P )
	- a .ps or a .pdf viewer (or both). There are numerous - the most famous 
are gv / Ghostview for .ps and Adobe Acrobat Reader for .pdf.
\end{itemize}

All these things can be installed very easily - just do it the way you 
install any other program.

Once all is installed, let's write our first LaTeX document.

3. LaTeX Hello World
3.1 The document

We will look at a very simple document :

\documentclass[a4paper, 11pt]{article}
\title{My first \LaTeX{} Document}
\author{Isabelle HURBAIN}
\begin{document}
\maketitle
Hello, world !
\end{document}


You can copy and past it into, for example, a new text file named 
"helloworld.tex".

3.2 Compilation
Save it, and compile it. To compile the file, just cd to the directory you 
saved it into and type

latex helloworld.tex

in a console tool (or a command in Windows).

You should see some lines displaying, and three new files are created.

	- The .dvi file is the output of the compilation - what you can view and print
	- The .aux file is an auxilliary file, which contains things like section 
numbers and figures numbers, so that it is possible to create a table of 
contents or a table of figures
	- The .log file is the file that logs all LaTeX information - LaTeX 
compilation messages and errors.

3.3 Visualization
Your LaTeX distribution probably provides a way to visualize .dvi files - 
in teTeX there is xdvi, in MiKTeX there is YAP...

If you find a .dvi viewer, you can directly view the helloworld.dvi file. 
If not, you can use

dvips helloworld.dvi

to get a ps file, or

dvipdfm helloworld.dvi

to get a pdf file. (dvipdfm can be slightly different, you may also find 
dvipdf or others).

Nice isn't it ?

3.4 Explanations
Let's look a bit more at the code. As you can see, all the commands begin 
with a \ ; \ is a special character in LaTeX that can be typed with 
$\backslash$. The other special characters ({, }, $, %, # and _) can be 
obtained with \{, \}, \$, \%, \# and \_. A \\ breaks the line (but you 
should not use this much as we'll see later).

The first line,

\documentclass[a4paper, 11pt]{article}

is very important. It tells LaTeX several things. \documentclass{article} 
tells it to follow an article layout. There are other layouts, such as 
report, book, letter... which we will discover in this course.  The a4paper 
and 11pt in the square brackets are general parameters of the document. I 
will talk about it a bit further in these courses. It tells LaTeX that the 
result will be printed onto A4 paper, with a text character size of 11 
points. Of course, if you use letter paper, feel free to replace a4paper 
with letterpaper.

\title{My first \LaTeX{} Document}

defines the title of the document, with a lovely LaTeX symbol.

\author{Isabelle HURBAIN}

defines the author of the document.

\begin{document}

is the signal for LaTeX that the real document begins. The lines before 
this line are called the preamble of the document.


\maketitle

is a command to create a title from the information in the preamble 
(typically \title and \author, eventually \date).

After that, you can compose the document (here just ``Hello, world !''), 
and finish it by

\end{document}.

It is true that it seems a bit complicated for just that. However, look how 
simple it was to create the title...

4. In the next course
In the next course, we will learn how to create a real-world document, with 
titles and sections, and I will explain more deeply the form-content 
dissociation advantages.


From isabelle.hurbain@free.fr  Tue Aug 13 05:59:02 2002
Return-Path: <isabelle.hurbain@free.fr>
Delivered-To: courses@linuxchix.org
Received: from mout0.freenet.de (mout0.freenet.de [194.97.50.131])
	by www.linuxchix.org (Postfix) with ESMTP id ABBE3272C66
	for <courses@linuxchix.org>; Tue, 13 Aug 2002 05:59:01 +1000 (EST)
Received: from [194.97.50.138] (helo=mx0.freenet.de)
	by mout0.freenet.de with esmtp (Exim 4.05)
	id 17eLL5-0002L9-00
	for courses@linuxchix.org; Mon, 12 Aug 2002 21:58:55 +0200
Received: from b148a.pppool.de ([213.7.20.138] helo=linux)
	by mx0.freenet.de with smtp (Exim 4.05 #1)
	id 17eLL1-0002N7-00
	for courses@linuxchix.org; Mon, 12 Aug 2002 21:58:52 +0200
Date: Mon, 12 Aug 2002 21:59:31 +0200
From: Isabelle HURBAIN <isabelle.hurbain@free.fr>
To: courses@linuxchix.org
Message-Id: <20020812215931.601087de.isabelle.hurbain@free.fr>
X-Mailer: Sylpheed version 0.7.5 (GTK+ 1.2.10; i686-pc-linux-gnu)
Mime-Version: 1.0
Content-Type: text/plain; charset=US-ASCII
Content-Transfer-Encoding: 7bit
Subject: [Courses] [LaTeX] Course 2 : a real-world document
Sender: courses-admin@linuxchix.org
Errors-To: courses-admin@linuxchix.org
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.0.1
Precedence: bulk
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
List-Archive: <http://mailman.linuxchix.org/pipermail/courses/>
Content-Length: 7368
Lines: 131

Hello everyone,

Here is the second chapter of the LaTeX course, explaining sectioning, and most of what you will need for a "basic" document.

This is text only ; you can have the PDF/TeX/DVI version on my website : 
<http://www.isabelle-hurbain.com/repository/latex/courses/>.

Best regards,
Isa - Balise



LaTeX courses
Course 2 : A real-world document
Isabelle HURBAIN 
isabelle.hurbain@free.fr

1. The different types of documents
2. Accents and linebreaks
3. Real-world document HOWTO
4. Exercise
5. In the next course



1. The different types of documents

In course 1, we made a "Hello, world" document, with only a title and a "Hello, world". We will now see how to make a real document.
There are mainly three wide-used classes of documents: book, report, and article. There are two other classes, namely slides and letters, but we won't talk about them yet.

article is for articles in journals, short reports, programs documentation...
report is for reports containing several chapters or parts.
book is for real books.

The rule I use (it's not a general rule, just what I do ;-) ) is to use the article class below 6-8 pages, report between 8 and 50 pages, and book for more than 50 pages. Needless to say, I pratically never use book :-)

2. Accents and linebreaks

When I first began to use LaTeX, the first things I wondered were "But why do my accents do not display?" and "How do I break a line?"
You can obtain accents by adding the line
\usepackage[latin1]{inputenc}
in the preamble of your document.
And to break a line, you will have to break the line twice in your source file, like this:

This is a line break
that do not works.

But this one works.

Here is the proof.


To edit a document in an another language that english, you may also want to use the babel package. Babel manages the redifinitions of titles (to get Chapitre instead of Chapter in a french document for example) and some typographical rules (like the beginning of section tabulation, existing in french but not in english).
To use the babel package, add to your preamble
\usepackage[language]{babel}
where language is the language of your document. For example, you wille have
\usepackage[french]{babel}
in french and
\usepackage[german]{babel}
in german.


3. Real-world document HOWTO
A generic document would contain a title, a table of contents, an introduction, several sections, a conclusion, and appendixes. The parts can contain subsections.

3.1 The title
The title is defined for the whole document. Its parameters are given in the document's preamble (before \begin{document}) with the commands \title, \author and \date. These commands take an unique argument (between curly braces {}).
As you must have guessed, \title defines the title, \author defines the author (in most cases, that would be yourself), and \date defines the date of writing or of publishing of the document.
If \date is omitted, the date will be the one of the last compilation. To avoid this behaviour, you can use \date{} to define a void date.
Once the parameters are in the preamble, you just have to insert a \maketitle following \begin{document} to display the title.
The title will have an whole page for classes report and book, and will be in the beginning of the first page for an article.

3.2 The sectionning commands
There are 6 sectionning commands in an article (in hierarchical order):
 - \part
 - \section
 - \subsection
 - \subsubsection
 - \paragraph
 - \subparagraph

Report and book also have a \chapter command, between part and section.

These commands take one argument, the title of the sectionning item. It can also take an optional argument (between square brackets []). This optional argument defines an alternative title, for example for the table of contents.

These commands manage the style of the title and its numbering. So for example, if you have a section foo before a section bar and if you decide that bar should be indeed before foo, just move your section. LaTeX takes care of the numbering of your sections (and following subsections) and of the update of the table of contents. Fantastic, isn't it?

3.3 The table of contents
The table of contents can be obtained with \verb+\tableofcontents+. It displays, in order of apparition, a title and a page number for each sectionning command lesser than \verb+tocdepth+. \verb+tocdepth+ is an intern variable, corresponding to these values :

\part (book and report) : -1 
\part (article) : 0
\chapter (book and report) : 0 
\section : 1
\subsection : 2
\subsubsection : 3
\paragraph : 4
\subparagraph : 5

The depth of the table of contents can be changed with \setcounter{tocdepth}{n} where n is the level of depth you want.

To update a table of contents after you modified something in the sectioning, you'll have to run latex twice on you document. Why? In fact, LaTeX maintains a separate file for the table of contents, the .toc file. At the first compilation, this file is updated, at the second, the .dvi document is updated.

3.4 The starred commands

The so-called starred commands are \chapter*, \section*, etc. They are sectioning commands, but special ones. They do not update the table of contents, they are not numerotated, and they do not update the chapter, section, etc. counter.
So what's their interest? They are indeed mainly used for introduction and conclusion. The introduction, for example, is rarely called "Chapter 1 - Introduction" in a book.
You might want, however, that your introduction is not numerotated (so you would use the starred command) but appears in the table of contents (so you wouldn't).
To achieve this, you may do something like this:
\chapter*{Introduction}
\addcontentsline{toc}{chapter}{\numberline{}Introduction}

The \chapter*{Introduction} creates the title of the introduction, with no numerotation as it was desired.
The \addcontentsline{toc}{chapter}{\numberline{}Introduction} add a line in the table of contents to create the Introduction entry.

The same kind of trick can be used for the conclusion.

3.5 The appendixes

To define that you left the plain document and that you are now writing appendixes, just use the command \appendix. This sets the sections counters to 0 and change the numbering system to alpha. It also redefines, for book and report, the title of chapters from "Chapter" to "Appendix". So if you create a chapter foo after the command \appendix, you will have "Appendix A  foo" as a title instead of "Chapter n+1  foo".

The appendixes are also entered in the table of contents.

4. Exercise
Now you have all instructions to create a basic document. Try to create one, experiment all the command described here, enjoy ;-)

Do not try to modify the aspect of what you get right now. Remember, LaTeX uses a content-oriented markup (a logical markup), not a form-oriented markup. If you want to do a chapter, do not put it as a part because you feel it is nicer - we will see how to modify the chapter definition if you need it. But, if the logical structure of your document requires a chapter, put a \chapter+. It will also have the advantage to make you think of your document's structure more deeply, and thus making clearer documents. And it highly simplifies the maintenance of your work.

5. In the next course

In the next course we will speak about tables and images and how to integrate them into a document.

From isabelle.hurbain@free.fr  Tue Dec 31 02:41:16 2002
Return-Path: <isabelle.hurbain@free.fr>
Delivered-To: courses@linuxchix.org
Received: from localhost (localhost [127.0.0.1])
	by www.linuxchix.org (Postfix) with ESMTP id 515F3273093
	for <courses@linuxchix.org>; Tue, 31 Dec 2002 02:41:16 +1100 (EST)
Received: from mel-rto3.wanadoo.fr (smtp-out-3.wanadoo.fr [193.252.19.233])
	by www.linuxchix.org (Postfix) with ESMTP id 1360E273079
	for <courses@linuxchix.org>; Tue, 31 Dec 2002 02:41:15 +1100 (EST)
Received: from mel-rta10.wanadoo.fr (193.252.19.193) by mel-rto3.wanadoo.fr (6.7.015)
        id 3E0C33B50012BF89 for courses@linuxchix.org; Mon, 30 Dec 2002 16:41:13 +0100
Received: from isa-laptop.local (217.128.5.44) by mel-rta10.wanadoo.fr (6.7.015)
        id 3E075B5C00234DDB for courses@linuxchix.org; Mon, 30 Dec 2002 16:41:13 +0100
Date: Mon, 30 Dec 2002 16:42:54 +0100
From: Isabelle HURBAIN <isabelle.hurbain@free.fr>
To: courses@linuxchix.org
Message-Id: <20021230164254.076bb349.isabelle.hurbain@free.fr>
X-Mailer: Sylpheed version 0.8.6 (GTK+ 1.2.10; i586-pc-linux-gnu)
Mime-Version: 1.0
Content-Type: text/plain; charset=US-ASCII
Content-Transfer-Encoding: 7bit
X-Virus-Scanned: by AMaViS new-20020517
Subject: [Courses] [LaTeX] Course 3: Tables and Images
Sender: courses-admin@linuxchix.org
Errors-To: courses-admin@linuxchix.org
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.0.1
Precedence: bulk
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
List-Archive: <http://mailman.linuxchix.org/pipermail/courses/>
Content-Length: 563
Lines: 14

Hi everyone,

Everything happens... even new chapters of LaTeX courses ;o)
Sorry it took me so long, I've had a huge load of work since September and I just couldn't take the time to put my ideas on the keyboard.

The lesson is available at :
<http://www.isabelle-hurbain.com/repository/latex/courses/en/index.php>
It is currently only in English, didn't take time to translate it in French - I will do it eventually.

Please let me know if things are unclear and/or badly written - my English seems to go further and further each day :-(

Best Regards

Isabelle

From terri@ostraya.zone12.com  Fri Jan 10 18:40:36 2003
Return-Path: <terri@ostraya.zone12.com>
Delivered-To: courses@linuxchix.org
Received: from localhost (localhost [127.0.0.1])
	by www.linuxchix.org (Postfix) with ESMTP id 6E3A827314E
	for <courses@linuxchix.org>; Fri, 10 Jan 2003 18:40:36 +1100 (EST)
Received: from berlinr.sprint.ca (berlinr.sprint.ca [209.5.194.99])
	by www.linuxchix.org (Postfix) with ESMTP id 7BEAE27310C
	for <courses@linuxchix.org>; Fri, 10 Jan 2003 18:40:34 +1100 (EST)
Received: from ostraya ([149.99.169.193]) by berlinr.sprint.ca
          (InterMail vM.5.01.02.00 201-253-122-103-101-20001108) with ESMTP
          id <20030110074028.JCZQ16795.berlinr.sprint.ca@ostraya>
          for <courses@linuxchix.org>; Fri, 10 Jan 2003 02:40:28 -0500
Received: from terri by ostraya with local (Exim 3.35 #1 (Debian))
	id 18Wtr5-0001GZ-00
	for <courses@linuxchix.org>; Fri, 10 Jan 2003 02:45:27 -0500
Date: Fri, 10 Jan 2003 02:45:26 -0500
From: Terri Oda <terri@zone12.com>
To: courses@linuxchix.org
Subject: Re: [Courses] [LaTeX] Course 3: Tables and Images
Message-ID: <20030110074526.GC3801@ostraya.zone12.com>
Mail-Followup-To: courses@linuxchix.org
References: <20021230164254.076bb349.isabelle.hurbain@free.fr>
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
In-Reply-To: <20021230164254.076bb349.isabelle.hurbain@free.fr>
User-Agent: Mutt/1.3.28i
X-Virus-Scanned: by AMaViS new-20020517
Sender: courses-admin@linuxchix.org
Errors-To: courses-admin@linuxchix.org
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.0.1
Precedence: bulk
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
List-Archive: <http://mailman.linuxchix.org/pipermail/courses/>
Content-Length: 1527
Lines: 37

I just wanted to say publically that I *really* appreciate the LaTeX course
information that's been posted so far.  

When the course started off it gave me just enough push to actually learn
some latex. (It's embarassing, really, that I didn't do it sooner, given
that I'm a mathematician and all.) That first lesson seemed so nice and easy
that I really had no excuse! 

Since then, I've actually used it a fair bit, including the paper I'm
working on now.  I'm still learning and it's been a little frustrating
typesetting a paper when I keep having to look up commands, but ultimately I
think it's made setting this particular paper a whole lot easier.

So thanks, Isabelle, and keep up the great work!

 Terri

On Mon, Dec 30, 2002 at 04:42:54PM +0100, Isabelle HURBAIN wrote:
> Hi everyone,
> 
> Everything happens... even new chapters of LaTeX courses ;o)
> Sorry it took me so long, I've had a huge load of work since September and I just couldn't take the time to put my ideas on the keyboard.
> 
> The lesson is available at :
> <http://www.isabelle-hurbain.com/repository/latex/courses/en/index.php>
> It is currently only in English, didn't take time to translate it in French - I will do it eventually.
> 
> Please let me know if things are unclear and/or badly written - my English seems to go further and further each day :-(
> 
> Best Regards
> 
> Isabelle
> _______________________________________________
> Courses mailing list
> Courses@linuxchix.org
> http://mailman.linuxchix.org/mailman/listinfo/courses


From isabelle.hurbain@free.fr  Thu Oct 16 03:35:23 2003
Return-Path: <isabelle.hurbain@free.fr>
Delivered-To: courses@linuxchix.org
Received: from localhost (localhost [127.0.0.1])
	by www.linuxchix.org (Postfix) with ESMTP id EBA0F272D8D
	for <courses@linuxchix.org>; Thu, 16 Oct 2003 03:35:22 +1000 (EST)
Received: from www.linuxchix.org ([127.0.0.1])
	by localhost (nest [127.0.0.1]) (amavisd-new, port 10024) with SMTP
	id 32762-01 for <courses@linuxchix.org>;
	Thu, 16 Oct 2003 03:35:22 +1000 (EST)
Received: from postfix4-2.free.fr (postfix4-2.free.fr [213.228.0.176])
	by www.linuxchix.org (Postfix) with ESMTP id 2552B272D7C
	for <courses@linuxchix.org>; Thu, 16 Oct 2003 03:33:37 +1000 (EST)
Received: from isa.local (nas-cbv-4-62-147-140-183.dial.proxad.net
	[62.147.140.183])	by postfix4-2.free.fr (Postfix) with SMTP
	id E3734C3FF; Wed, 15 Oct 2003 19:32:20 +0200 (CEST)
Date: Wed, 15 Oct 2003 19:34:47 +0200
From: Isabelle Hurbain <isabelle.hurbain@free.fr>
To: courses@linuxchix.org
Message-Id: <20031015193447.48668578.isabelle.hurbain@free.fr>
X-Mailer: Sylpheed version 0.9.6claws (GTK+ 1.2.10; i686-pc-linux-gnu)
Mime-Version: 1.0
Content-Type: text/plain; charset=US-ASCII
Content-Transfer-Encoding: 7bit
X-Virus-Scanned: by amavisd-new-20030616-p5 (Debian) at linuxchix.org
X-Topics: LaTeX document-markup course
cc: koke@sindominio.net
Subject: [Courses] [LaTeX] Spanish translation, 4th chapter, new address
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.1
Precedence: list
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Archive: <http://linuxchix.org/pipermail/courses>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
X-List-Received-Date: Wed, 15 Oct 2003 17:35:23 -0000
Content-Length: 647
Lines: 18

Hello everyone,

Several things to say today:
1) I've been noticed yesterday that Jorge "Koke" Bernal has translated
the first three chapters of the course in Spanish, so feel free to look
at his page if you speak Spanish:
http://sindominio.net/koke/?s=trans

2) Plus, he has written a "parallel" fourth chapter about mathematical
expressions. This chapter is at the moment only available in Spanish at
http://sindominio.net/koke/?s=docs

3) I don't remember if I told you that the address of the courses had
slightly changed since I redesigned the website. It is now at:
http://www.isabelle-hurbain.com/latex/index.en.html

That's all folks!

Isa
From mary@home.puzzling.org  Sun Jun  6 10:39:17 2004
Return-Path: <mary@home.puzzling.org>
X-Original-To: courses@linuxchix.org
Delivered-To: courses@linuxchix.org
Received: from localhost (localhost [127.0.0.1])
	by www.linuxchix.org (Postfix) with ESMTP id 39BEF272F47
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 10:39:17 +1000 (EST)
Received: from www.linuxchix.org ([127.0.0.1])
	by localhost (nest [127.0.0.1]) (amavisd-new, port 10024) with SMTP
	id 02476-04 for <courses@linuxchix.org>;
	Sun, 6 Jun 2004 10:39:17 +1000 (EST)
Received: from mail.syd.swiftdsl.com.au (mail.syd.swiftdsl.com.au
	[202.154.83.58])
	by www.linuxchix.org (Postfix) with SMTP id BF677272F3B
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 10:39:16 +1000 (EST)
Received: (qmail 9444 invoked from network); 6 Jun 2004 00:39:11 -0000
Received: from unknown (HELO home.puzzling.org) (218.214.66.203)
  by mail.syd.swiftdsl.com.au with SMTP; 6 Jun 2004 00:39:11 -0000
Received: from localhost (flay [127.0.0.1])
	by home.puzzling.org (Postfix) with ESMTP id 917BD788350
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 10:39:13 +1000 (EST)
Received: from home.puzzling.org ([127.0.0.1])
	by localhost (flay [127.0.0.1]) (amavisd-new, port 10024) with ESMTP
	id 19490-02 for <courses@linuxchix.org>;
	Sun, 6 Jun 2004 10:39:13 +1000 (EST)
Received: from titus.home.puzzling.org (titus.home.puzzling.org [10.0.0.2])
	by home.puzzling.org (Postfix) with SMTP id 38FAE78834B
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 10:39:13 +1000 (EST)
Received: (nullmailer pid 8058 invoked by uid 1001);
	Sun, 06 Jun 2004 00:39:09 -0000
Date: Sun, 6 Jun 2004 10:39:09 +1000
From: Mary <mary-linuxchix@puzzling.org>
To: courses@linuxchix.org
Message-ID: <20040606003909.GD11789@titus.home.puzzling.org>
Mail-Followup-To: courses@linuxchix.org
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
X-Nihilism: Consistency is all I ask... Give us this day our daily mask.
X-GPG-Key: 1024D/77625870
X-GPG-Fingerprint: B141 CD1A 4603 1CD7 6D64  EFBF D256 C568 7762 5870
User-Agent: Mutt/1.5.5.1+cvs20040105i
X-Virus-Scanned: by amavisd-new-20030616-p7 (Debian) at home.puzzling.org
X-Virus-Scanned: by amavisd-new-20030616-p7 (Debian) at linuxchix.org
X-Topics: LaTeX document-markup course
	C programming course
	Python programming course 
	Networking course
	Perl programming course
Subject: [Courses] 
 Closing down old courses ( [C] [LaTeX] [Python] [Perl] [Networking] )
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.1
Precedence: list
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Archive: <http://linuxchix.org/pipermail/courses>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
X-List-Received-Date: Sun, 06 Jun 2004 00:39:17 -0000
Content-Length: 530
Lines: 14

Hi courses folk,

I'm closing down the old courses topics on this list: the ones for the
C, Python and Perl programming courses; the LaTeX course; and the
Networking course. None of these courses have had posts for a long time
and we don't want to confuse new subscribers into thinking they're still
running.

Archives of most of these courses are still available at
http://www.linuxchix.org/content/courses/

If anyone wants to restart them at some point we'll be happy to re-open
them: contact courses-owner@linuxchix.org

-Mary
From meredydd@everybuddy.com  Sun Jun  6 20:29:40 2004
Return-Path: <meredydd@everybuddy.com>
X-Original-To: courses@linuxchix.org
Delivered-To: courses@linuxchix.org
Received: from localhost (localhost [127.0.0.1])
	by www.linuxchix.org (Postfix) with ESMTP id 2C4BD272EB0
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 20:29:40 +1000 (EST)
Received: from www.linuxchix.org ([127.0.0.1])
	by localhost (nest [127.0.0.1]) (amavisd-new, port 10024) with SMTP
	id 08061-01 for <courses@linuxchix.org>;
	Sun, 6 Jun 2004 20:29:40 +1000 (EST)
Received: from cohost.lazzurs.net (cohost.lazzurs.net [69.10.133.52])
	by www.linuxchix.org (Postfix) with ESMTP id A9A99272E89
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 20:29:39 +1000 (EST)
Received: from cohost.lazzurs.net ([127.0.0.1] helo=localhost)
	by cohost.lazzurs.net with esmtp (Exim 4.22 and XAMS @VERSION@)
	id 1BWutz-0007Cu-8L
	for courses@linuxchix.org; Sun, 06 Jun 2004 10:29:19 +0000
From: Meredydd <meredydd@everybuddy.com>
To: courses@linuxchix.org
Subject: Re: [Courses]  Closing down old courses ( [C] [LaTeX] [Python] [Perl]
	[Networking] )
Date: Sun, 6 Jun 2004 11:28:49 +0100
User-Agent: KMail/1.6.2
References: <20040606003909.GD11789@titus.home.puzzling.org>
In-Reply-To: <20040606003909.GD11789@titus.home.puzzling.org>
MIME-Version: 1.0
Content-Disposition: inline
Content-Type: text/plain;
  charset="iso-8859-1"
Content-Transfer-Encoding: 7bit
Message-Id: <200406061128.49218.meredydd@everybuddy.com>
X-MailScanner-Information: Please contact the ISP for more information
X-MailScanner: Found to be clean
X-Virus-Scanned: by amavisd-new-20030616-p7 (Debian) at linuxchix.org
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.1
Precedence: list
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Archive: <http://linuxchix.org/pipermail/courses>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
X-List-Received-Date: Sun, 06 Jun 2004 10:29:40 -0000
Content-Length: 902
Lines: 25

Heya,
	Please don't close the filesystem topic, I'm working up a new lesson or 
two, including (at last) loopback and encrypted loopback...

Meredydd

On Sunday 06 June 2004 01:39, Mary wrote:
> Hi courses folk,
>
> I'm closing down the old courses topics on this list: the ones for
> the C, Python and Perl programming courses; the LaTeX course; and the
> Networking course. None of these courses have had posts for a long
> time and we don't want to confuse new subscribers into thinking
> they're still running.
>
> Archives of most of these courses are still available at
> http://www.linuxchix.org/content/courses/
>
> If anyone wants to restart them at some point we'll be happy to
> re-open them: contact courses-owner@linuxchix.org
>
> -Mary
> _______________________________________________
> Courses mailing list
> Courses@linuxchix.org
> http://mailman.linuxchix.org/mailman/listinfo/courses
From mary@home.puzzling.org  Sun Jun  6 21:57:05 2004
Return-Path: <mary@home.puzzling.org>
X-Original-To: courses@linuxchix.org
Delivered-To: courses@linuxchix.org
Received: from localhost (localhost [127.0.0.1])
	by www.linuxchix.org (Postfix) with ESMTP id E32EB272F91
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 21:57:04 +1000 (EST)
Received: from www.linuxchix.org ([127.0.0.1])
	by localhost (nest [127.0.0.1]) (amavisd-new, port 10024) with SMTP
	id 08978-07 for <courses@linuxchix.org>;
	Sun, 6 Jun 2004 21:57:04 +1000 (EST)
Received: from mail.syd.swiftdsl.com.au (mail.syd.swiftdsl.com.au
	[202.154.83.58])
	by www.linuxchix.org (Postfix) with SMTP id DAA62272F8A
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 21:56:31 +1000 (EST)
Received: (qmail 783 invoked from network); 6 Jun 2004 11:55:48 -0000
Received: from unknown (HELO home.puzzling.org) (218.214.66.203)
  by mail.syd.swiftdsl.com.au with SMTP; 6 Jun 2004 11:55:48 -0000
Received: from localhost (flay [127.0.0.1])
	by home.puzzling.org (Postfix) with ESMTP id 5E1B3788350
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 21:55:48 +1000 (EST)
Received: from home.puzzling.org ([127.0.0.1])
	by localhost (flay [127.0.0.1]) (amavisd-new, port 10024) with ESMTP
	id 22911-08 for <courses@linuxchix.org>;
	Sun, 6 Jun 2004 21:55:48 +1000 (EST)
Received: from titus.home.puzzling.org (titus.home.puzzling.org [10.0.0.2])
	by home.puzzling.org (Postfix) with SMTP id 3B3D878834B
	for <courses@linuxchix.org>; Sun,  6 Jun 2004 21:55:48 +1000 (EST)
Received: (nullmailer pid 29412 invoked by uid 1001);
	Sun, 06 Jun 2004 11:55:44 -0000
Date: Sun, 6 Jun 2004 21:55:44 +1000
From: Mary <mary-linuxchix@puzzling.org>
To: courses@linuxchix.org
Subject: Re: [Courses]  Closing down old courses ( [C] [LaTeX] [Python] [Perl]
	[Networking] )
Message-ID: <20040606115544.GF9169@titus.home.puzzling.org>
Mail-Followup-To: courses@linuxchix.org
References: <20040606003909.GD11789@titus.home.puzzling.org>
	<200406061128.49218.meredydd@everybuddy.com>
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
In-Reply-To: <200406061128.49218.meredydd@everybuddy.com>
X-Nihilism: All I ask is our common due... Give us this day our daily cue.
X-GPG-Key: 1024D/77625870
X-GPG-Fingerprint: B141 CD1A 4603 1CD7 6D64  EFBF D256 C568 7762 5870
User-Agent: Mutt/1.5.5.1+cvs20040105i
X-Virus-Scanned: by amavisd-new-20030616-p7 (Debian) at home.puzzling.org
X-Virus-Scanned: by amavisd-new-20030616-p7 (Debian) at linuxchix.org
X-BeenThere: courses@linuxchix.org
X-Mailman-Version: 2.1
Precedence: list
List-Id: List for courses run by LinuxChix volunteers <courses.linuxchix.org>
List-Help: <mailto:courses-request@linuxchix.org?subject=help>
List-Post: <mailto:courses@linuxchix.org>
List-Subscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=subscribe>
List-Archive: <http://linuxchix.org/pipermail/courses>
List-Unsubscribe: <http://mailman.linuxchix.org/mailman/listinfo/courses>,
	<mailto:courses-request@linuxchix.org?subject=unsubscribe>
X-List-Received-Date: Sun, 06 Jun 2004 11:57:05 -0000
Content-Length: 282
Lines: 8

On Sun, Jun 06, 2004, Meredydd wrote:
> Heya,
> 	Please don't close the filesystem topic, I'm working up a new lesson or 
> two, including (at last) loopback and encrypted loopback...

Only the topics in the subject line were shut down. Filesystem is [FS]
and is still there.

-Mary
