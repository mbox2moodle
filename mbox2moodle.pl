#!/usr/bin/perl -w
=pod

=head1 convert.pl

=head2 Description

    Converts a series of mbox files into moodle backup.zip files
    ready to be restored into moodle.

=head1 SYNOPSIS

 ./mbox2moodle.pl

     Options
     --sourcedir    DIRECTORY
     --sourcefile   FILE
     --outputdir    DIRECTORY
     --templatedir  DIRECTORY

=head1 Authors

    Penny Leach <penny@she.geek.nz>
    Julie Pichon <julie.pichon@gmx.com>

=cut

use diagnostics;
use open ':utf8';
use strict;
use warnings;

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Carp;
use Cwd;
use Data::Dumper;
use Encode;
use File::Basename;
use File::Path;
use FindBin;
use Getopt::Long;
use HTML::Mason;
use HTML::Mason::Compiler;
use Mail::Box::Manager;
use Mail::MboxParser;
use Mail::Message::Attachment::Stripper;
use Mail::Thread;
use Pod::Usage;
use Smart::Comments;
use Tie::Dir qw(DIR_UNLINK);

my $root = $FindBin::Bin;

my $mboxes = [];
my $sourcedir;
my $outputdir="$root/output";
my $templatedir="$root/templates";
my $forumdata = "moddata/forum/8000";
my $courses = [];
my $coursecounter = 0;
my $lessoncounter;
our $postcounter;
our $dummystarters;
our $dummies;

GetOptions(
    'sourcefile=s@', \@$mboxes,
    'sourcedir=s', \$sourcedir,
    'outputdir=s', \$outputdir,
    'templatedir=s', \$templatedir,
) or pod2usage;

pod2usage unless ((scalar(@{$mboxes}) > 0 or $sourcedir) and $outputdir);

$mboxes = [<$sourcedir*>] unless scalar(@{$mboxes}) > 0;

my $mgr = Mail::Box::Manager->new;

my $outbuf;           # Temp buffer used for writing output
my %mason_args = (
    'comp_root'        => $templatedir,
    'use_object_files' => 0,
    'static_source'    => 1,
    'out_method'       =>\$outbuf,
);

my $interp = HTML::Mason::Interp->new(%mason_args);
my $compiler = $interp->compiler;

foreach my $mbox (@{$mboxes}) {
    $outbuf = '';

    $postcounter    = 1;
    $lessoncounter  = 1;

    my $course = {
        'id'          => $coursecounter++,
        'name'        => basename($mbox),
	'lessons'     => [],
        'namelessons' => 0,
    };

    my $coursedir = "$outputdir/$course->{name}";

    my $folder  = $mgr->open(folder => $mbox);
    ### Parsing: $mbox
    $dummystarters = 0;

    my $threader = new Mail::Thread($folder->messages('ALL'));

    $threader->thread;

    for ($threader->rootset) { ### Evaluating |===[%]    |
	sort_out_messages($_, 0, $course, '', 0, "$coursedir/$forumdata");
    }

    $folder->close(write => 'NEVER');

    warn "this course had $dummystarters dummy thread starts "
        . "(out of " . scalar(@{$course->{lessons}}) . ") "
	. "and I tried to guess the first post.\n" if $dummystarters;

    # Sort lessons chronologically, so they appear in order on the course page
    sub by_timestamp {
	@{$a->{posts}}[0]->{timestamp} <=> @{$b->{posts}}[0]->{timestamp};
    }
    @{$course->{lessons}} = sort by_timestamp @{$course->{lessons}};

    $interp->exec('/moodle.xml', course => $course);
    mkpath "$coursedir" unless -d "$coursedir";
    open my $out, '>', "$coursedir/moodle.xml" or croak "Output error: $!";
    print $out $outbuf;
    close $out;

    # zip it all up
    my $zip = Archive::Zip->new();
    $zip->addTree("$coursedir", '');

    unless ( $zip->writeToFileNamed("backup-$course->{name}.zip") == AZ_OK ) {
       croak "zip write error ($!)";
    }
    print "written to backup-$course->{name}.zip\n";
}

exit 0;

sub sort_out_messages {
    my ($post, $level, $course, $lesson, $parentid, $path) = @_;

    if (!$post->message) {
	$dummystarters++;

	my $newtop = guess_top_post($post->children) if $post->children;
	if (!$newtop->message) { next; }

	$post->remove_child($newtop);

	foreach my $child ($post->children) {
	    $newtop->add_child($child);
	}

	$post = $newtop;
    }

    # the first message in a thread is a special case
    # it needs to be both a lesson and a forum post.
    if ($level == 0) {
	$lesson = {
	    'id'    => $lessoncounter++,
	    'name'  => strip_subject($post->message->head->get("Subject")),
	    'body'  => parse_body($post->message),
	    'posts' => [],
	};

	$lesson->{summary} = (split(/\n\n/, $lesson->{body}))[0];

	if (!$lesson->{summary}) {
	    $lesson->{summary} = $lesson->{name};
	}

	push @{$course->{lessons}}, $lesson;
    }

    # Create post
    my $newpost = {
	'parent'    => $parentid,
	'id'        => $postcounter++,
	'subject'   => strip_subject($post->message->head->get("Subject")),
	'body'      => parse_body($post->message),
	'timestamp' => $post->message->timestamp,
    };

    $newpost->{attachment} = parse_attachments($post->message,
					       "$path/$newpost->{id}");
    push @{$lesson->{posts}}, $newpost;

    sort_out_messages($post->child, $level+1, $course, $lesson, "$newpost->{id}", $path) if $post->child;
    sort_out_messages($post->next, $level, $course, $lesson, "$newpost->{parent}", $path) if $post->next;
}


# Only take relevant content type
sub parse_body {
    my $message = shift;
    my $body = '';

    if ($message->isMultipart()) {
	foreach my $part ($message->parts) {
	    if ((!defined($part->contentType)) ||
		(lc($part->contentType) eq 'text/html') ||
		(lc($part->contentType) eq 'text/plain')) {
		$body .= $part->decoded->string;
	    }
	}
    } else {
	my $content_type = $message->get('Content-Type') || 'text/plain';
	if (!defined($content_type) ||
	    (lc($content_type) eq 'text/html') ||
	    (lc($content_type) eq 'text/plain')) {
	    $body = $message->decoded->string;
	}
    }

    $body;
}

sub parse_attachments {
    my ($message, $path) = @_;
    my $name = '';

    my $stripper = Mail::Message::Attachment::Stripper->new($message);
    my @attachments = $stripper->attachments;

    my $count = count_valid_attachments(@attachments);

    if ($count > 0) {
	# Extract attachments from message
	foreach my $attach (@attachments) {
	    if (($attach->{content_type} ne 'text/html') &&
		($attach->{content_type} ne 'application/pgp-signature')) {
		mkpath($path);
		open my $out_file, '>:raw', "$path/$attach->{filename}"  or croak "Output error: ($!)";
		print $out_file $attach->{payload};
		close $out_file;

		$name = $attach->{filename};
	    }
	}

	# Moodle only allows 1 attachment/post, if >1 zip them up
	if ($count > 1) {
	    $name = "attachments.zip";

	    my $zip = Archive::Zip->new();
	    $zip->addTree("$path");

	    # If the script is re-run, remove old attachments archive
	    if ($zip->memberNamed($name)) {
		$zip->removeMember($name);
	    }

	    unless ($zip->overwriteAs("$path/$name") ==  AZ_OK) {
		croak "attachment zip write error ($!)";
	    }

	    # Remove unzipped attachments
	    tie my %dir, 'Tie::Dir', $path, DIR_UNLINK;
	    foreach (keys %dir) {
		if (($_ !~ /^$name$/) && ($_ ne ".") && ($_ ne "..")) {
		    delete $dir{$_};
		}
	    }
	}
    }

    $name;
}

sub count_valid_attachments {
    my @attachments = @_;
    my $count = 0;

    foreach my $attach (@attachments) {
	if (($attach->{content_type} ne 'text/html') &&
	    ($attach->{content_type} ne 'application/pgp-signature')) {
	    $count += 1;
	}
    }

    $count;
}

sub strip_subject {
    $_ = shift;
    s/\[Courses\]\s*//;
    $_;
}

sub guess_top_post {
    my (@children) = @_;

    my $guess;
    my @candidates;

    foreach my $child (@children) {
	if ($child->message) {
	    if (!$child->isreply) {
		push @candidates, $child;
	    }
	}
    }

    if (@candidates == 0) {
	push @candidates, @children;
    }

    if (@candidates == 1) {
	$guess = $candidates[0];
    } else {
	my @round2;

	# Is there a 're:' in the subject?
	foreach my $cand (@candidates) {
	    if (index(lc($cand->message->head->get("Subject")), 're:') == -1) {
		push @round2, $cand;
	    }
	}

	if (@round2 == 0) {
	    push @round2, @children;
	}

	if (@round2 == 1) {
	    $guess = $round2[0];
	} else {
	    # Last resort, pick the one with the oldest timestamp
	    my $oldest = pop @round2;
	    foreach my $msg (@round2) {
		if ($msg->message->timestamp < $oldest->message->timestamp) {
		    $oldest = $msg;
		}
	    }
	    $guess = $oldest;
	}
    }

    $guess;
}
